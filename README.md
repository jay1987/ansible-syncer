# Ansible Role: ansible-syncer

This role use ansible synchronize module to deal with many kinds of file transfer jobs.

## Variables

  - remote_path: remote site path (required)
  - local_path: local backup folder (required)
  - local_tmp_dir: local tmp folder (required)
  - log_path: rsync transfer logs (required)
  - local_latest_link: make a soft link points to the latest backup folder (optional)
  - exclude_path: exclude some files (optional)

## Examples

```yaml
- hosts: localhost
  tasks:
    - name: mirror the public repo
      include_role:
        name: ansible-syncer
      vars:
        remote_path: rsync://mirrors.tuna.tsinghua.edu.cn/centos
        local_path: /data/mirrors/www/pub/centos
        local_tmp_dir: /data/mirrors/tmp/sync_centos
        log_path: /data/mirrors/logs/centos.log
```

```yaml
- hosts: remote-hosts
  tasks:
    - name: daily incremental backup
      delegate_to: localhost
      include_role:
        name: ansible-syncer
      vars:
        remote_path: /data/service/user-data
        local_path: /backup/daily/{{ date_today | default(ansible_date_time.date) }}/user-data
        local_latest_link: /backup/latest_link/user-data
        local_tmp_dir: /backup/tmp/sync-user-data
        log_path: /backup/logs/user-data.log
        exclude_path: 
          - /analytics-logs/*
          - /caches/*
          - /export/*
          - /import/*
          - /log/*
          - /logs/*
          - /tmp/*
```
